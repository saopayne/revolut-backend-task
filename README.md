## Candidate: Ademola Oyewale

Date: 01-12-2019

## Revolut Backend Test Task
Design and implement a RESTful API (including data model and the backing implementation) for
money transfers between accounts.

## Running the project
To build and run the project, execute the following from a project root directory

```
$ gradlew build
$ java -jar build/libs/ademola-backend-test-1.0-SNAPSHOT.jar
```

Or run the already built jar in the root directly:
```
$ java -jar ademola-backend-test-1.0-SNAPSHOT.jar
```

#### Testing the project
Test files are located in "src/integration-test".

The tests are run as part of the pipeline when the server is being started.


### How does my solution address the challenge?

In my solution, I looked at transfers as a transactions log which would be modeled as an append-only log.

When a transfer is initiated between two user accounts, the transfer doesn't move the money from source to destination immediately but rather submits a task entry and the status 
of the transaction can be updated asynchronously. The transaction shouldn't be synchronous as this will not easily yield itself to scaling in the future.

There are different transaction statuses dependent on a state a transaction can be in:

`submitted` - The transaction is pending in internet banking.  It has not yet been received by the banking system.

`accepted` - The transaction has been received and will be processed manually.

`pendingApproval` - The transaction is pending for approval from other User(s) - applicable only for accounts with multiple signatures.

`running` - when transaction processing has started

`completed` - The transaction has been completed successfully.

`failed` - The transaction failed to be executed due to an abnormal error such as system failure, future transactions that fail to be executed due to unavailability of funds.

`expired` - The transaction has expired because not all required users have approved it by the end of its execution date.

### Technologies Used

- Java as implementation language.
- Grizzly as the HTTP server.
- Javax WS RS API for RESTful Web Services
- Jackson for JSON serialization and deserialization.
- JUnit for unit tests.

### Further improvements/suggestions

- Avoid double charging. To avoid double charging customers, there should be a time slot during which repeated transactions
  will provide a guarantee that only one goes through. There's flexibility on how can be implemented but the simpler approach is to give a time buffer.
   - Another option is to go with Idempotency key from the clients and have them pass along with the request.

- It's an in-memory storage and thus, all the data is lost on application restart. This should have a actual database to back it up.

- Event-based architecture, using such an architecture will be more useful as we can work with choreography rather than orchestration.

- Command Query Responsibility Segregation. This can be improved to separate the models that read from those that update.

- State Machine. Using a state machine will make all the possible states a transaction can move to(transitions) be more visible and a suitable workflow
  can be designed around that.
  
- Improve upon concurrency, the concurrency can be impproved to manage multiple instances of the server/replicas spawn and to prevent data race problem in a distributed system.
  A centralized data store maybe?

- Classify Error responses as “retryable” or “non-retryable” to improve customer experience.

- Responses are still very lean, transactions for example should return timestamps, channels through which the transaction was done. 

### Rest API

Base URL: `http://localhost:8080` Postman or CURL works :)

Set Header `Content-Type: application/json` to avoid unsupported media type (415) error.

**Available endpoints**

|Method                             | Description               |
|-----------------------------------|---------------------------|
| GET  /accounts/{accountId}        | Get account by its ID     |
| POST /accounts/                   | Create a new account      |
| POST  /accounts/{id}/withdraw     | Withdraw from account     |
| POST  /accounts/{id}/deposit      | Deposit into account      |
| DELETE /accounts/{id}/            | Delete an account         |
| POST /transfer/                   | Transfer funds            |
| GET  /transactions/               | Show transactions history |
| GET  /transactions/{id}           | Get transaction by its ID |


##### A. Accounts

##### 1. Create Account

` POST /accounts` 

Payload:

```{"initialAmount": 10000}```
 
###### Expected Response

```
{
    "id": "70525c6b-a25c-41ba-9fe6-56c3d8fa3c09",
    "balance": 10000,
    "date": 1575214594218
}
```

##### 2. Get Account by ID

`GET /accounts/{ID}`

###### Expected Response

```
{
    "id": "5dd510db-8880-4fce-aff8-ba2c4d6ce5f4",
    "balance": 20000,
    "date": 1575214594218
}
```

#### 3. List all accounts 

`GET /accounts`

###### Expected Response

```
[
    {
        "id": "5dd510db-8880-4fce-aff8-ba2c4d6ce5f4",
        "balance": 20000,
        "date": 1575214594218
    },
    {
        "id": "70525c6b-a25c-41ba-9fe6-56c3d8fa3c09",
        "balance": 10000,
        "date": 1575214594218
    }
]
```

##### 4. Withdraw from Account 

` POST /accounts/{ID}/withdraw`
 
Payload:
```
{
    "amount": 10000
}
``` 

##### Expected Response
```
{
    "id": "0a37549c-a600-400e-a8ea-9b48b406c968",
    "balance": 10000
}
```

##### 5. Deposit into Account 

` POST /accounts/{ID}/deposit`

Payload:
 ```
 {
     "amount": 10000
 }
 ``` 

##### Expected Response
```
{
    "id": "0a37549c-a600-400e-a8ea-9b48b406c968",
    "balance": 40000,
    "date": 1575214594218
}
```

##### 6. Delete Account By ID 

`DELETE /accounts/{ID}`

##### Expected Response

```
Account with ID:{ID} deleted
```

#### B. Transfer

###### 1. Create transfer 

`POST /transfers` with body:
```
{
  "sourceAccountId":"6b9f393a-95f4-4659-b2cf-8cdab345865a",
  "destinationAccountId":"0d982fd9-0d2e-4072-b659-16de570e18ab",
  "amount": 5000
}
```

##### Expected Response
```
{
    "id": "ba618738-b751-4c0c-a9c8-1203d0312b78",
    "sourceId": "6b9f393a-95f4-4659-b2cf-8cdab345865a",
    "destinationId": "0d982fd9-0d2e-4072-b659-16de570e18ab",
    "amount": 5000,
    "status": "COMPLETED",
    "failureReason": null
}
--------
{
    "id": "413ac3e4-4680-4d11-937d-b691403964c0",
    "sourceId": "6b9f393a-95f4-4659-b2cf-8cdab345865a",
    "destinationId": "0d982fd9-0d2e-4072-b659-16de570e18ab",
    "amount": 5000,
    "status": "PENDING_APPROVAL",
    "failureReason": null
}
--------
{
    "id": "5eb27753-d9c1-4ff6-affd-3c43622f8fb5",
    "sourceId": "6b9f393a-95f4-4659-b2cf-8cdab345865a",
    "destinationId": "0d982fd9-0d2e-4072-b659-16de570e18ab",
    "amount": 5000,
    "status": "FAILED",
    "failureReason": "Insufficient funds"
}
```


#### C. Transactions

###### 1. Get All Transactions 

`GET /transactions`

##### Expected Response
```
[
    {
        "id": "bc577a51-b373-472e-9a7b-d2430386c338",
        "sourceId": "6b9f393a-95f4-4659-b2cf-8cdab345865a",
        "destinationId": "0d982fd9-0d2e-4072-b659-16de570e18ab",
        "amount": 5000,
        "status": "FAILED",
        "failureReason": "Insufficient funds",
        "date": 1575214594218
    },
    {
        "id": "c0686e19-8372-4419-b9d0-41f6f2df9e29",
        "sourceId": "6b9f393a-95f4-4659-b2cf-8cdab345865a",
        "destinationId": "0d982fd9-0d2e-4072-b659-16de570e18ab",
        "amount": 5000,
        "status": "COMPLETED",
        "failureReason": null,
        "date": 1575214594218
    },
    {
        "id": "ccbef879-3067-48ef-81dd-18086932c1f9",
        "sourceId": "6b9f393a-95f4-4659-b2cf-8cdab345865a",
        "destinationId": "0d982fd9-0d2e-4072-b659-16de570e18ab",
        "amount": 5000,
        "status": "FAILED",
        "failureReason": "Insufficient funds",
        "date": 1575214594218
    }
]
```

###### 2. Get A Transaction

`GET /transactions/{ID}`

##### Expected Response
```
{
    "id": "bc577a51-b373-472e-9a7b-d2430386c338",
    "sourceId": "6b9f393a-95f4-4659-b2cf-8cdab345865a",
    "destinationId": "0d982fd9-0d2e-4072-b659-16de570e18ab",
    "amount": 5000,
    "status": "FAILED",
    "failureReason": "Insufficient funds",
    "date": 1575214594218
}
```