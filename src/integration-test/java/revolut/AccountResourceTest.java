package revolut;

import revolut.dao.AccountDao;
import revolut.exception.banking.InsufficientFundsException;
import revolut.exception.banking.NoSuchAccountException;
import revolut.model.Account;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.WebTarget;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountResourceTest extends BaseTest {

    private WebTarget client;

    @Before
    public void setUp() {
        client = baseClient
                .path("/accounts");
    }

    @Test
    public void testCreateAccount() {
        BigDecimal initialAmount = BigDecimal.valueOf(30000);

        Account account = createAccount(initialAmount);

        Assert.assertNotNull(account);
        Assert.assertNotNull(account.getId());
        Assert.assertThat(account.getBalance(), Is.is(initialAmount));
    }

    @Test
    public void testGetAccountById() {
        Account account = createAccount(BigDecimal.ZERO);

        Account accountById = getAccountById(account.getId());

        Assert.assertThat(accountById.getId(), Is.is(account.getId()));
        Assert.assertThat(accountById.getBalance(), Is.is(account.getBalance()));
    }

    @Test
    public void testGetAllAccounts() {
        List<Account> createdAccounts = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            createdAccounts.add(createAccount(BigDecimal.valueOf(i)));
        }

        List<Account> accounts = getAccounts();

        Assert.assertThat(accounts.size(), Is.is(createdAccounts.size()));
        Assert.assertTrue(accounts.containsAll(createdAccounts));
    }

    @Test
    public void testDeleteAccount() {
        Account account = createAccount(BigDecimal.ZERO);

        client.path(account.getId()).request().delete();

        List<Account> accounts = getAccounts();
        Assert.assertThat(accounts.size(), Is.is(0));
    }

    @Test
    public void testWithdrawFromAccount() throws InsufficientFundsException, NoSuchAccountException {
        BigDecimal initialAmount = BigDecimal.valueOf(10000);
        BigDecimal amountToWithdraw = BigDecimal.valueOf(5000);

        AccountDao accountDao = new AccountDao();

        // create an account to test with
        Account account = accountDao.create(initialAmount);

        // withdraw half of the total balance from the account
        accountDao.withdraw(account.getId(), amountToWithdraw);
        Assert.assertEquals(account.getBalance(), initialAmount.subtract(amountToWithdraw));

        // withdraw another half of the total balance from the account which brings the balance to zero
        accountDao.withdraw(account.getId(), amountToWithdraw);
        Assert.assertEquals(account.getBalance(), BigDecimal.ZERO);
    }

    @Test(expected = InsufficientFundsException.class)
    public void testInsufficientFundsException() throws InsufficientFundsException, NoSuchAccountException {
        BigDecimal initialAmount = BigDecimal.valueOf(10000);
        BigDecimal amountToWithdraw = BigDecimal.valueOf(11000);

        AccountDao accountDao = new AccountDao();
        // create an account to test with
        Account account = accountDao.create(initialAmount);

        // attempt a withdrawal that's more than the balance
        accountDao.withdraw(account.getId(), amountToWithdraw);
    }
}
