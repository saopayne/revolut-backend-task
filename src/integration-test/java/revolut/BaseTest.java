package revolut;

import revolut.app.Application;
import revolut.model.Account;
import revolut.resource.requests.CreateAccountRequest;
import org.junit.After;
import org.junit.Before;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.List;

public abstract class BaseTest {

    WebTarget baseClient = ClientBuilder.newClient().target("http://localhost:8080");
    private Application application;

    @Before
    public void setUpBaseTest() {
        application = new Application();
        application.run();
    }

    @After
    public void tearDownBaseTest() {
        application.shutDown();
    }

    Account createAccount(BigDecimal initialBalance) {
        return baseClient
                .path("accounts")
                .request()
                .post(Entity.entity(new CreateAccountRequest(initialBalance),
                        MediaType.APPLICATION_JSON), Account.class);
    }

    Account getAccountById(String id) {
        return baseClient
                .path("accounts/" + id)
                .request(MediaType.APPLICATION_JSON)
                .get(Account.class);
    }


    List<Account> getAccounts() {
        return baseClient
                .path("accounts")
                .request(MediaType.APPLICATION_JSON)
                .get()
                .readEntity(new GenericType<List<Account>>() {
                });
    }

}
