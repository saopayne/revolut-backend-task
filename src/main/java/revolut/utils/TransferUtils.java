package revolut.utils;

import revolut.exception.banking.InsufficientFundsException;
import revolut.exception.banking.InvalidAmountException;
import revolut.model.Account;

import java.math.BigDecimal;
import java.util.Objects;

public class TransferUtils {

    public synchronized void transferMoney(Account source, Account destination, BigDecimal amount)
            throws InsufficientFundsException, InvalidAmountException {
        if (Objects.equals(source.getId(), destination.getId())) {
            throw new IllegalArgumentException("Source and destination accounts can't be the same.");
        }

        if (source.getBalance().compareTo(amount) < 1) {
            throw new InsufficientFundsException();
        }

        if (amount.compareTo(BigDecimal.ZERO) < 1) {
            throw new InvalidAmountException("The amount should be greater than zero");
        }

        withdraw(source, amount);
        deposit(destination, amount);
    }

    private void withdraw(Account account, BigDecimal amount) throws InsufficientFundsException {
        if (account.getBalance().compareTo(amount) < 1) {
            throw new InsufficientFundsException();
        }
        account.setBalance(account.getBalance().subtract(amount));
    }

    private void deposit(Account account, BigDecimal amount) {
        account.setBalance(account.getBalance().add(amount));
    }

}
