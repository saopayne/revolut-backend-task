package revolut.exception.banking;

public class NoSuchTransactionException extends BankingException {

    @Override
    public String getMessage() {
        return "No such transaction";
    }

}
