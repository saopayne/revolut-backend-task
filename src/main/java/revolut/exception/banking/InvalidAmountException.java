package revolut.exception.banking;

public class InvalidAmountException extends BankingException {

    private String message;

    public InvalidAmountException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
