package revolut.exception.banking;

public class InsufficientFundsException extends BankingException {

    public InsufficientFundsException() {
    }

    @Override
    public String getMessage() {
        return "Insufficient funds";
    }
}
