package revolut.resource;

import revolut.exception.banking.NoSuchTransactionException;
import revolut.model.Transaction;
import revolut.service.TransferService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/transactions")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource {

    @Inject
    private TransferService service;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransaction(@PathParam("id") String id) {
        try {
            Transaction transaction = service.getTransaction(id);
            return Response.ok(transaction).build();
        } catch (NoSuchTransactionException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransactions() {
        return Response.ok(service.getTransactions()).build();
    }

}
