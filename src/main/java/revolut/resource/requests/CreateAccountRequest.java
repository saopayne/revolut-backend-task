package revolut.resource.requests;

import java.math.BigDecimal;

public class CreateAccountRequest {
    private BigDecimal initialAmount;

    public CreateAccountRequest() {
    }

    public CreateAccountRequest(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }
}
