package revolut.resource.requests;

import java.math.BigDecimal;

public class WithdrawRequest {

    private BigDecimal amount;

    public WithdrawRequest() {
    }

    public WithdrawRequest(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

}
