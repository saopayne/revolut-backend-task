package revolut.resource;


import revolut.exception.banking.InsufficientFundsException;
import revolut.exception.banking.InvalidAmountException;
import revolut.exception.banking.NoSuchAccountException;
import revolut.model.Account;
import revolut.resource.requests.CreateAccountRequest;
import revolut.resource.requests.DepositRequest;
import revolut.resource.requests.WithdrawRequest;
import revolut.service.TransferService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    @Inject
    private TransferService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccounts() {
        return Response.ok(service.getAccounts()).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccountById(@PathParam("id") String id) {
        Account account;
        try {
            account = service.getAccountById(id);
        } catch (NoSuchAccountException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(account).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAccount(CreateAccountRequest request) {
        Account account = service.createAccount(request.getInitialAmount());
        return Response.ok(account, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccountById(@PathParam("id") String id) {
        try {
            service.deleteAccount(id);
        } catch (NoSuchAccountException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok("Account with ID:" + id + " deleted", MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("/{id}/withdraw")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response withdrawFromAccount(@PathParam("id") String id, WithdrawRequest request) {
        try {
            Account account = service.withdrawFromAccount(id, request.getAmount());
            return Response.ok(account, MediaType.APPLICATION_JSON).build();
        } catch (NoSuchAccountException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InsufficientFundsException e) {
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @POST
    @Path("/{id}/deposit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response depositIntoAccount(@PathParam("id") String id, DepositRequest request) {
        try {
            Account account = service.depositIntoAccount(id, request.getAmount());
            return Response.ok(account, MediaType.APPLICATION_JSON).build();
        } catch (NoSuchAccountException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InvalidAmountException e) {
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

}

