package revolut.resource;


import revolut.resource.requests.TransferRequest;
import revolut.service.TransferService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/transfers")
public class TransferResource {

    @Inject
    private TransferService service;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transferMoney(TransferRequest request) {
        return Response.ok(service.createTransferRequest(request.getSourceAccountId(),
                request.getDestinationAccountId(),
                request.getAmount())).build();
    }
}
