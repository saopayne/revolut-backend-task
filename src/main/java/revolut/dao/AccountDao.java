package revolut.dao;

import revolut.exception.banking.InsufficientFundsException;
import revolut.exception.banking.InvalidAmountException;
import revolut.exception.banking.NoSuchAccountException;
import revolut.model.Account;

import java.math.BigDecimal;
import java.util.*;

public class AccountDao {

    private Map<String, Account> accounts;

    public AccountDao() {
        this.accounts = new HashMap<>();
    }

    public Account create(BigDecimal initialBalance) {
        Account account = new Account(UUID.randomUUID().toString(), initialBalance);
        account.setDate(new Date());
        accounts.put(account.getId(), account);
        return account;
    }

    public void delete(String id) throws  NoSuchAccountException {
        validateId(id);
        accounts.remove(id);
    }

    public Account deposit(String id, BigDecimal amount) throws InvalidAmountException, NoSuchAccountException {
        validateId(id);
        if (amount.compareTo(BigDecimal.ZERO) < 1) {
            throw new InvalidAmountException("The amount should be greater than zero.");
        }
        Account account = accounts.get(id);
        account.setBalance(account.getBalance().add(amount));
        return account;
    }

    public Account withdraw(String id, BigDecimal amount) throws NoSuchAccountException, InsufficientFundsException {
        validateId(id);
        Account account = accounts.get(id);
        if (account.getBalance().compareTo(amount) < 0) {
            throw new InsufficientFundsException();
        }
        account.setBalance(account.getBalance().subtract(amount));
        return account;
    }

    public List<Account> getAll() {
        return new ArrayList<>(accounts.values());
    }

    public Account get(String id) throws NoSuchAccountException {
        validateId(id);
        return accounts.get(id);
    }

    private void validateId(String id) throws NoSuchAccountException {
        if (id == null || !accounts.containsKey(id)) {
            throw new NoSuchAccountException();
        }
    }

}
