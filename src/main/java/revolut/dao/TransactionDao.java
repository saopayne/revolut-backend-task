package revolut.dao;

import revolut.exception.banking.NoSuchTransactionException;
import revolut.model.Transaction;

import java.math.BigDecimal;
import java.util.*;

public class TransactionDao {

    private Map<String, Transaction> transactions = new HashMap<>();

    public Transaction create(String sourceId, String destinationId, BigDecimal amount) {
        String id = UUID.randomUUID().toString();
        Transaction transaction = new Transaction(id, sourceId, destinationId, amount);
        transaction.setDate(new Date());
        transactions.put(id, transaction);
        return transaction;
    }

    public Transaction get(String id) throws NoSuchTransactionException {
        validateId(id);
        return transactions.get(id);
    }

    public List<Transaction> getAll() {
        return new ArrayList<>(transactions.values());
    }

    private void validateId(String id) throws NoSuchTransactionException {
        if (id == null || !transactions.containsKey(id)) {
            throw new NoSuchTransactionException();
        }
    }

}
