package revolut.service;

import revolut.dao.AccountDao;
import revolut.dao.TransactionDao;
import revolut.exception.banking.InsufficientFundsException;
import revolut.exception.banking.InvalidAmountException;
import revolut.exception.banking.NoSuchAccountException;
import revolut.exception.banking.NoSuchTransactionException;
import revolut.model.Account;
import revolut.model.Transaction;
import revolut.model.TransactionStatus;
import revolut.utils.TransferUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Handles all transfers across various accounts
 */
public class TransferService {

    private AccountDao accounts;
    private TransactionDao transactions;

    private Executor executor = Executors.newSingleThreadExecutor();
    private ReentrantLock lock = new ReentrantLock();
    private TransferUtils utils = new TransferUtils();

    public TransferService(AccountDao accounts, TransactionDao transactions) {
        this.accounts = accounts;
        this.transactions = transactions;
    }

    public List<Account> getAccounts() {
        return accounts.getAll();
    }

    public Account getAccountById(String id) throws NoSuchAccountException {
        return accounts.get(id);
    }

    public Account createAccount(BigDecimal initialBalance) {
        try {
            lock.lock();
            return accounts.create(initialBalance);
        } finally {
            lock.unlock();
        }
    }

    public void deleteAccount(String id) throws NoSuchAccountException {
        try {
            lock.lock();
            accounts.delete(id);
        } finally {
            lock.unlock();
        }
    }

    public Account withdrawFromAccount(String id, BigDecimal amount) throws NoSuchAccountException, InsufficientFundsException {
        try {
            lock.lock();
            return accounts.withdraw(id, amount);
        } finally {
            lock.unlock();
        }
    }

    public Account depositIntoAccount(String id, BigDecimal amount) throws InvalidAmountException, NoSuchAccountException {
        try {
            lock.lock();
            return accounts.deposit(id, amount);
        } finally {
            lock.unlock();
        }
    }

    public Transaction getTransaction(String transactionId) throws NoSuchTransactionException {
        return transactions.get(transactionId);
    }

    public List<Transaction> getTransactions() {
        return transactions.getAll();
    }

    public Transaction createTransferRequest(String sourceAccountId, String destinationAccountId, BigDecimal amount) {
        Transaction transaction = transactions.create(sourceAccountId, destinationAccountId, amount);
        transaction.setStatus(TransactionStatus.SUBMITTED);
        Runnable task = createTransactionTask(transaction);
        executor.execute(task);
        transaction.setStatus(TransactionStatus.PENDING_APPROVAL);
        return transaction;
    }

    /**
     * It's responsible for creating a transaction task entry and for updating the statuses based
     * on the result of execution of various checks and updates.
     * @param transaction: the transaction to create a task for.
     * @return
     */
    private Runnable createTransactionTask(Transaction transaction) {
        return () -> {
                try {
                    lock.lock();
                    transaction.setStatus(TransactionStatus.RUNNING);
                    Account source = accounts.get(transaction.getSourceId());
                    Account destination = accounts.get(transaction.getDestinationId());
                    utils.transferMoney(source, destination, transaction.getAmount());
                    transaction.setStatus(TransactionStatus.COMPLETED);
                } catch (NoSuchAccountException nsa) {
                    transaction.setStatus(TransactionStatus.FAILED);
                    transaction.setFailureReason(nsa.getMessage());
                } catch (InsufficientFundsException ise) {
                    transaction.setStatus(TransactionStatus.FAILED);
                    transaction.setFailureReason(ise.getMessage());
                } catch (InvalidAmountException iae) {
                    transaction.setStatus(TransactionStatus.FAILED);
                    transaction.setFailureReason(iae.getMessage());
                } finally {
                    lock.unlock();
                }
            };
    }
}
