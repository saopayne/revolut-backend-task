package revolut.model;

import java.math.BigDecimal;
import java.util.Date;

public class Transaction {

    private String id;
    private String sourceId;
    private String destinationId;
    private BigDecimal amount;
    private Date date;

    private TransactionStatus status;
    private String failureReason;

    public Transaction() {
    }

    public Transaction(String id, String sourceId, String destinationId, BigDecimal amount) {
        this.id = id;
        this.sourceId = sourceId;
        this.destinationId = destinationId;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setFailureReason(String reason) {
        this.failureReason = reason;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", destinationId='" + destinationId + '\'' +
                ", amount=" + amount +
                ", status=" + status +
                ", failureReason='" + failureReason + '\'' +
                ", date='" + date + '\'' +
                '}';
    }

}
