package revolut.model;

public enum TransactionStatus {
    SUBMITTED("submitted"),
    ACCEPTED("accepted"),
    PENDING_APPROVAL("pending_approval"),
    RUNNING("running"),
    COMPLETED("completed"),
    FAILED("failed"),
    EXPIRED("expired");

    String name;

    TransactionStatus(String name) {
        this.name = name;
    }

}