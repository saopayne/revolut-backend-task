package revolut.app;

import revolut.dao.AccountDao;
import revolut.dao.TransactionDao;
import revolut.resource.AccountResource;
import revolut.resource.TransactionResource;
import revolut.resource.TransferResource;
import revolut.service.TransferService;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ext.RuntimeDelegate;
import java.io.IOException;

public class Application {

    private HttpServer server;

    public static void main(String[] args) throws IOException {
        Application application = new Application();
        application.run();
        System.out.println("Application has started. Press 'Enter' to exit");
        System.in.read();
        application.shutDown();
    }

    public void run() {
        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.register(JacksonFeature.class);
        resourceConfig.register(TransferResource.class);
        resourceConfig.register(AccountResource.class);
        resourceConfig.register(TransactionResource.class);
        resourceConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(new TransferService(new AccountDao(), new TransactionDao())).to(TransferService.class);
            }
        });

        HttpHandler endpoint = RuntimeDelegate.getInstance().createEndpoint(resourceConfig, HttpHandler.class);
        server = HttpServer.createSimpleServer("http://localhost", 8080);
        server.getServerConfiguration().addHttpHandler(endpoint);
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shutDown() {
        server.shutdown();
    }
}